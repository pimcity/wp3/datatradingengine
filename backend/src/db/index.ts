import {
  CompanyInformation, DataOffer, IndividualUser, CompanyUser, Migration,
} from './models';
import DynamoStore from './DynamoStore';
import logger from '../utils/logger';

export const individualUsersStore = new DynamoStore<IndividualUser>('IndividualUsers');
export const companyUsersStore = new DynamoStore<CompanyUser>('CompanyUser');
export const companiesInformationStore = new DynamoStore<CompanyInformation>('CompaniesInformation');
export const dataOffersStore = new DynamoStore<DataOffer>('DataOffers', undefined,
  [
    { AttributeName: 'id', AttributeType: 'S' },
    { AttributeName: 'companyId', AttributeType: 'S' },
  ],
  [
    {
      IndexName: 'data-offers-companyId-index',
      KeySchema: [
        { KeyType: 'HASH', AttributeName: 'companyId' },
      ],
      Projection: { ProjectionType: 'ALL' },
      ProvisionedThroughput: {
        ReadCapacityUnits: 10,
        WriteCapacityUnits: 10,
      },
    },
  ]);

export const migrations = new DynamoStore<Migration>('Migrations');

export const initStores = async () => {
  logger.info('Initializing stores...');

  await Promise.all([
    individualUsersStore.init(),
    companyUsersStore.init(),
    companiesInformationStore.init(),
    dataOffersStore.init(),
    migrations.init(),
  ]);

  logger.info('Stores initialized!');
};
