/* eslint-disable */
"use strict";

import dotenv from 'dotenv';

import { Webhook } from '../types/Webhook';

Object.defineProperty(exports, "__esModule", {
  value: true
});

dotenv.config({ path: process.env.ENV_PATH || '.env' })

const { env } = process;

const config = {
  app: {
    name: env.npm_package_name,
    version: env.npm_package_version
  },
  env: env.NODE_ENV,
  debug: env.DEBUG === 'true',
  port: env.PORT,
  host: env.HOST,
  log: {
    error: env.ERROR_LOG,
    combined: env.COMBINED_LOG,
  },
  aws: {
    dynamo: {
      region: env.AWS_DYNAMO_REGION,
      accessKeyId: env.AWS_DYNAMO_ACCESS_KEY_ID,
      secretAccessKey: env.AWS_DYNAMO_SECRET_KEY_ID,
      endpoint: env.AWS_DYNAMO_ENDPOINT,
    },
  },
  queue: {
    connection: {
      host: env.QUEUE_CONNECTION_HOST as string,
      port: Number(env.QUEUE_CONNECTION_PORT),
    },
  },
  services: {
    DataProvenance: {
      url: env.SERVICES_DATA_PROVENANCE_URL as string,
    },
    DVTMP: {
      url: env.SERVICES_DVTMP_URL as string,
    },
    DVTUP: {
      url: env.SERVICES_DVTUP_URL as string,
    },
    PCM: {
      url: env.SERVICES_PCM_URL as string,
      apiKey: env.SERVICES_PCM_API_KEY as string,
    },
    PDS: {
      url: env.SERVICES_PDS_URL as string,
      clientId: env.SERVICES_PDS_CLIENT_ID as string,
      clientSecret: env.SERVICES_PDS_CLIENT_SECRET as string,
    },
    UP: {
      url: env.SERVICES_UP_URL as string,
    },
    TM: {
      url: env.SERVICES_TM_URL as string,
      apiKey: env.SERVICES_TM_API_KEY as string,
    },
  },
  security: {
    apiKeyHashes: JSON.parse(env.API_KEY_HASHES as string) as Array<string>,
    keycloak: {
      clientId: env.KEYCLOAK_CLIENT_ID as string,
      serverUrl: env.KEYCLOAK_SERVER_URL as string,
      realm: env.KEYCLOAK_REALM as string,
      realmPublicKey: env.KEYCLOAK_REALM_PUBLIC_KEY as string,
    },
  },
  dataOffers: {
    pointsPerUser: Number(env.OFFERS_POINTS_PER_USERS),
  },
  developers: {
    webhooks: JSON.parse(env.WEBHOOKS as string) as Array<Webhook>,
  }
};

export default config;
