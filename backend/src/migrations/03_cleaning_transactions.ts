import {
  DataOffer,
  IndividualUser,
  Transaction,
} from '../db/models';
import { individualUsersStore } from '../db';
import DynamoStore from '../db/DynamoStore';

const backupUsers = async (users: Array<IndividualUser>) => {
  const individualUsersStoreBackup = new DynamoStore<IndividualUser>('IndividualUsers-migration-03-backup');
  await individualUsersStoreBackup.init();
  await individualUsersStoreBackup.batchStore(users);
};

const cleanTransactions = (transactions: Array<Transaction>) => {
  const offersSeen: Record<DataOffer['id'], boolean> = {};
  return transactions.filter((t) => {
    if (t.type !== 'dataOffer') return true;
    const seen = offersSeen[t.metadata.offerId];
    offersSeen[t.metadata.offerId] = true;
    return !seen;
  });
};

const run = async () => {
  const users = await individualUsersStore.list();
  await backupUsers(users);

  const migratedUsers = users.map((user) => ({
    ...user,
    history: {
      transactions: cleanTransactions(user.history.transactions),
    },
  }));

  await individualUsersStore.batchStore(migratedUsers);
};

export default run;
