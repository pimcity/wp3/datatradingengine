import { dataOffersStore } from '../db';
import { DataOffer } from '../db/models';

type DepDataOffer = DataOffer | {
  status: 'dataAvailable';
}

const run = async () => {
  const dataOffers = (await dataOffersStore.list()) as Array<DepDataOffer>;

  const migratedOffers = dataOffers.map((offer) => ({
    ...offer,
    status: offer.status === 'dataAvailable' ? 'data-available' : offer.status,
  }) as DataOffer);

  await dataOffersStore.batchStore(migratedOffers);
};

export default run;
