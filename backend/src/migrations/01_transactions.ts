import { v4 as uuidv4 } from 'uuid';

import {
  DataOfferTransactionMetadata,
  IndividualUser,
  OtherTransactionMetadata,
  TaskTransactionMetadata,
  Transaction,
} from '../db/models';
import { dataOffersStore, individualUsersStore } from '../db';

interface DeprecatedTransaction {
  id: string;
  type: 'dataOffer' | 'task' | 'other';
  credits: number;
  createdAt: number;
}

const buildMetadata = async (
  t: DeprecatedTransaction,
): Promise<DataOfferTransactionMetadata | TaskTransactionMetadata | OtherTransactionMetadata> => {
  if (t.type === 'dataOffer') {
    const dataOffer = await dataOffersStore.fetch(t.id);
    return {
      offerId: t.id,
      dataTypes: dataOffer.dataTypes,
      purpose: dataOffer.purpose,
    };
  }
  if (t.type === 'task') {
    return {
      taskId: t.id,
      taskName: t.id,
    };
  }
  return { description: `Earned ${t.credits} points!` };
};

const migrateTransactions = async (
  transactions: Array<DeprecatedTransaction>,
): Promise<Array<Transaction>> => Promise.all(transactions.map(async (t) => ({
  id: uuidv4(),
  type: t.type,
  points: t.credits,
  createdAt: t.createdAt,
  metadata: await buildMetadata(t),
}) as Transaction));

const run = async () => {
  const users = await individualUsersStore.list();

  const migratedUsers: Array<IndividualUser> = await Promise.all(users.map(async (user) => ({
    ...user,
    history: {
      transactions: await migrateTransactions(
        user.history.transactions as unknown as Array<DeprecatedTransaction>,
      ),
    },
  })));

  await individualUsersStore.batchStore(migratedUsers);
};

export default run;
