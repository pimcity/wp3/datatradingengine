import { dataOffersStore } from '../db';

const removeCompanyOffers = async (companyId: string) => {
  const offers = await dataOffersStore.queryByIndex('companyId', companyId);
  await Promise.all(offers.map(({ id }) => dataOffersStore.delete(id)));
};

const run = async () => {
  await Promise.all([
    removeCompanyOffers('f5785e09-ad49-4ee2-ad50-585b4f3174d4'),
    removeCompanyOffers('8ea67434-6301-4784-8d5e-afbef6f3e392'),
  ]);
};

export default run;
