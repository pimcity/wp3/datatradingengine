import { Queue } from 'bullmq';
import R from 'ramda';

import config from '../config';
import { dataOffersStore } from '../db';
import { CreateOfferQueue } from '../operations/dataOffers/createDataOffer/definitions';

const run = async () => {
  const queue: CreateOfferQueue = new Queue('create-offers', {
    defaultJobOptions: {
      attempts: 5,
      backoff: {
        type: 'exponential',
        delay: 1000,
      },
    },
    connection: config.queue.connection,
  });

  const jobs = await queue.getJobs('failed');
  const [jobsToRemove, jobsToRetry] = R.partition((j) => !j.data.values.offer && j.name !== 'init-offer', jobs);

  await Promise.all(jobsToRemove.map((j) => j.remove()));

  const offersToKeep = jobsToRetry.reduce((acc, j) => (
    {
      ...acc,
      ...(j.data.values.offer ? { [j.data.values.offer.id]: true } : {}),
    }
  ),
  {} as Record<string, boolean>);

  const offers = await dataOffersStore.list();
  const proms = offers.filter((o) => !offersToKeep[o.id] && o.status !== 'data-available').map((o) => dataOffersStore.delete(o.id));
  await Promise.all(proms);
};

export default run;
