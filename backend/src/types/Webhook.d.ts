export type WebhookEventID = 'end-users.points.change';

export interface Webhook {
  endpoint: string;
  eventId: WebhookEventID;
}
