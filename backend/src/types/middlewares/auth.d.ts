import { Request } from 'express';

export interface AuthUser {
  id: string;
  token: string;
}

export interface AuthRequest extends Request {
  user: AuthUser;
}
