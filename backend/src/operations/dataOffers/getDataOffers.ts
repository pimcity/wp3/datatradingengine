import { DataOffer, OfferStatus } from '../../db/models';
import { dataOffersStore, individualUsersStore } from '../../db';
import { ForbiddenError } from '../../utils/error';

const getOffersMap = async (userId: string) => {
  const user = await individualUsersStore.safeFetch(userId);
  return user?.history.transactions.filter(
    (t) => t.type === 'dataOffer',
  ).reduce(
    (acc, t) => ({ ...acc, [t.id]: true }),
    {} as { [offerId: string]: boolean },
  );
};

export const getIndividualDataOffers = async (userId: string): Promise<Array<DataOffer>> => {
  const offersMap = await getOffersMap(userId);
  if (!offersMap) return [];

  const dataOffers = await dataOffersStore.list();
  return dataOffers.filter(
    (dataOffer) => offersMap[dataOffer.id],
  ).map(({ data, ...offer }) => offer);
};

export const getIndividualDataOffersMissed = async (userId: string): Promise<Array<DataOffer>> => {
  const [offersMap, dataOffers] = await Promise.all([
    getOffersMap(userId),
    dataOffersStore.list(),
  ]);

  if (!offersMap) return dataOffers;

  return dataOffers.filter(
    (dataOffer) => !offersMap[dataOffer.id],
  ).map(({ data, ...offer }) => offer);
};

const filterOffers = (
  offers: Array<DataOffer>,
  include?: Array<OfferStatus>,
  exclude?: Array<OfferStatus>,
) => {
  if (!include && !exclude) return offers;
  if (include) return offers.filter((o) => include.includes(o.status));
  return offers.filter((o) => !exclude?.includes(o.status));
};

export const getCompanyDataOffers = async (
  companyId: string,
  { include, exclude }: { include?: Array<OfferStatus>; exclude?: Array<OfferStatus>; },
): Promise<Array<DataOffer>> => {
  const offers = await dataOffersStore.queryByIndex('companyId', companyId);
  const filteredOffers = filterOffers(offers, include, exclude);
  return filteredOffers.map(({ data, ...offer }) => offer);
};

export const getCompanyDataOfferData = async (
  companyId: string,
  offerId: string,
): Promise<any> => {
  const offer = await dataOffersStore.fetch(offerId);
  if (offer.companyId !== companyId) {
    throw new ForbiddenError('Offer does not belong to company');
  }
  return offer.data;
};
