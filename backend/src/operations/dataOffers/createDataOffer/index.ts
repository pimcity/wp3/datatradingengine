import config from '../../../config';
import { DataOfferSubmission } from '../../../db/models';
import servicesSingleton from '../../../services/servicesSingleton';

import { initQueue } from './queue';

const services = servicesSingleton.getServices();
const { queue: createOfferQueue } = initQueue('create-offers', config.queue.connection, services);

const createDataOffer = async (
  submission: DataOfferSubmission, jwtToken: string,
): Promise<void> => {
  await createOfferQueue.add(
    'init-offer',
    {
      submission,
      jwtToken,
      values: {},
    },
  );
};

export default createDataOffer;
