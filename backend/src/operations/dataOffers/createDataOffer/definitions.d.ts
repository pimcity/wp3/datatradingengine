import { Queue, Worker } from 'bullmq';
import {
  DataOffer, DataOfferSubmission, DataPackage, DataUser, OfferStatus,
} from '../../../db/models';
import { Services } from '../../../services/definitions';

export type CreateOfferJobName = 'init-offer' | 'get-size-of-audience' | 'get-consents' | 'get-profiles' | 'filter-by-audience' | 'pick-random-profiles' | 'get-data-packages' | 'watermark-data' | 'update-end-users' | 'update-data-buyer' | 'update-offer';

export interface CreateOfferJobValues {
  offer: DataOffer;
  price: number;
  sizeOfAudience: number;
  usersIdsWithConsents: Array<string>;
  profiles: Array<DataUser>;
  dataPackages: Array<DataPackage>;
  creditsSpent: number;
}

export type CreateOfferJobReturnValue = Promise<Partial<CreateOfferJobValues>>;

export interface CreateOfferJobData {
  submission: DataOfferSubmission;
  jwtToken: string;
  values: Partial<CreateOfferJobValues>;
}

export interface CreateOfferJobOpData extends CreateOfferJobData {
  services: Services;
}

export interface CreateOfferJobDescription {
  handler: (params: CreateOfferJobOpData) => CreateOfferJobReturnValue;
  nextStatus: OfferStatus | null;
  nextJob: CreateOfferJobName | Array<CreateOfferJobName> | null;
}

export type CreateOfferQueue = Queue<
  CreateOfferJobData, Partial<CreateOfferJobValues>, CreateOfferJobName
>;

export type CreateOfferWorker = Worker<
  CreateOfferJobData, Partial<CreateOfferJobValues>, CreateOfferJobName
>;
