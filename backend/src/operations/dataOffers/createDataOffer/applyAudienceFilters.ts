import differenceInYears from 'date-fns/differenceInYears';
import parseISO from 'date-fns/parseISO';

import {
  Audience, Country, DataUser, Education,
  Gender, Interest, Job, NumericFilter,
} from '../../../db/models';

const getAge = (birthDateStr?: string) => {
  if (!birthDateStr) return undefined;
  const birthDate = parseISO(birthDateStr);
  return differenceInYears(Date.now(), birthDate);
};

const applyNumericFilter = (
  num?: number, filter?: NumericFilter,
): boolean => !filter
  || (!!num && (!filter.min || filter.min <= num) && (!filter.max || filter.max >= num));

const applyCategoryFilter = <T>(category?: T | Array<T>, filter?: Array<T>): boolean => {
  if (!filter) return true;
  if (!category) return false;

  if (Array.isArray(category)) return category.every((c) => filter.includes(c));
  return filter.includes(category);
};

const applyAudienceFilters = async (
  users: Array<DataUser>, audience: Audience,
) => users.filter((user) => [
  applyNumericFilter(getAge(user.socioDemographics?.birthDate), audience.age),
  applyNumericFilter(user.socioDemographics?.householdMembers, audience.householdMembers),
  applyNumericFilter(user.socioDemographics?.income, audience.income),
  applyCategoryFilter<Country>(user.socioDemographics?.country, audience.country),
  applyCategoryFilter<Education>(user.socioDemographics?.education, audience.education),
  applyCategoryFilter<Gender>(user.socioDemographics?.gender, audience.gender),
  applyCategoryFilter<Job>(user.socioDemographics?.job, audience.job),
  applyCategoryFilter<Interest>(user.interests, audience.interest),
].every((pred) => pred));

export default applyAudienceFilters;
