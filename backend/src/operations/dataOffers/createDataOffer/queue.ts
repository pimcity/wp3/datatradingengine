import { ConnectionOptions, Queue, QueueScheduler } from 'bullmq';

import { Services } from '../../../services/definitions';
import logger from '../../../utils/logger';

import { CreateOfferQueue } from './definitions';
import initWorkers from './workers';

export const retryDeadJobs = async (queue: CreateOfferQueue) => {
  const jobs = await queue.getJobs('failed');
  logger.info(`Dead Jobs: ${jobs.map((j) => `${j.name}: ${j.data.values.offer?.status} (${j.timestamp})`).join(', ')}`);
  return jobs.filter((j) => !!j.data.values.offer).map((j) => j.retry());
};

export const initQueue = (queueName: string, connection: ConnectionOptions, services: Services) => {
  const queue: CreateOfferQueue = new Queue(queueName, {
    defaultJobOptions: {
      attempts: 5,
      backoff: {
        type: 'exponential',
        delay: 1000,
      },
    },
    connection,
  });
  const queueScheduler = new QueueScheduler(queueName, { connection });
  initWorkers(queue, connection, services);

  retryDeadJobs(queue).catch((err) => logger.error(`Error retrying failed jobs: ${err}`));
  // queue.retryJobs().catch((err) => logger.error(`Error retrying failed jobs: ${err}`));

  return { queue, queueScheduler };
};
