import { ConnectionOptions, Worker } from 'bullmq';
import R from 'ramda';

import { DataOffer, OfferStatus } from '../../../db/models';
import { dataOffersStore } from '../../../db';
import { Services } from '../../../services/definitions';
import logger from '../../../utils/logger';

import {
  CreateOfferJobDescription,
  CreateOfferJobName,
  CreateOfferQueue,
  CreateOfferWorker,
} from './definitions';

import {
  initOffer, filterByAudience, getConsents, getDataPackages,
  getProfiles, getSizeOfAudience, pickRandomProfiles, updateOffer,
  updateEndUsers, updateDataBuyer, watermarkData,
} from './jobs';

const jobDescriptions: Record<CreateOfferJobName, CreateOfferJobDescription> = {
  'init-offer': {
    handler: initOffer,
    nextStatus: 'getting-price-and-size',
    nextJob: 'get-size-of-audience',
  },
  'get-size-of-audience': {
    handler: getSizeOfAudience,
    nextStatus: 'getting-consents',
    nextJob: 'get-consents',
  },
  'get-consents': {
    handler: getConsents,
    nextStatus: 'getting-profiles',
    nextJob: 'get-profiles',
  },
  'get-profiles': {
    handler: getProfiles,
    nextStatus: 'applying-audience-filters',
    nextJob: 'filter-by-audience',
  },
  'filter-by-audience': {
    handler: filterByAudience,
    nextStatus: 'fitting-audience-in-budget',
    nextJob: 'pick-random-profiles',
  },
  'pick-random-profiles': {
    handler: pickRandomProfiles,
    nextStatus: 'getting-data-packages',
    nextJob: 'get-data-packages',
  },
  'get-data-packages': {
    handler: getDataPackages,
    nextStatus: 'watermarking-data',
    nextJob: 'watermark-data',
  },
  'watermark-data': {
    handler: watermarkData,
    nextStatus: 'updating-accounts',
    nextJob: ['update-end-users', 'update-data-buyer'],
  },
  'update-end-users': {
    handler: updateEndUsers,
    nextStatus: null,
    nextJob: null,
  },
  'update-data-buyer': {
    handler: updateDataBuyer,
    nextStatus: 'data-available',
    nextJob: 'update-offer',
  },
  'update-offer': {
    handler: updateOffer,
    nextStatus: 'data-available',
    nextJob: null,
  },
};

const storeOffer = async (offer: DataOffer, status: OfferStatus | null) => {
  const storingOffer = { ...offer, status: status || offer.status };

  const existingOffer = await dataOffersStore.safeFetch(offer.id);
  return existingOffer
    ? dataOffersStore.upsert(offer.id, storingOffer)
    : dataOffersStore.store(storingOffer);
};

const overrideOffer = (
  originalOffer?: DataOffer, resultOffer?: DataOffer,
): DataOffer | undefined => {
  if (!originalOffer) return resultOffer;
  if (!resultOffer) return originalOffer;
  return { ...originalOffer, ...resultOffer } as DataOffer | undefined;
};

const initWorkers = (
  queue: CreateOfferQueue, connection: ConnectionOptions, services: Services, numOfWorkers = 1,
) => {
  const workers = R.range(0, numOfWorkers).map(() => {
    const worker: CreateOfferWorker = new Worker(
      queue.name,
      async (job) => jobDescriptions[job.name].handler({ ...job.data, services }),
      { connection },
    );

    worker.on('completed', async (job, result) => {
      const { nextStatus, nextJob } = jobDescriptions[job.name];
      const { offer: originalOffer } = job.data.values;
      const { offer: resultOffer } = result;
      const offer = overrideOffer(originalOffer, resultOffer);

      if (offer && (offer !== originalOffer || nextStatus)) {
        if (logger.isDebugEnabled()) {
          logger.debug(`Saving offer: ${JSON.stringify(offer)}`);
        }
        await storeOffer(offer, nextStatus);
      }
      if (nextJob) {
        const nextJobs = Array.isArray(nextJob) ? nextJob : [nextJob];
        nextJobs.map((jobName) => queue.add(
          jobName,
          { ...job.data, values: { ...job.data.values, ...result, offer } },
        ));
      }
    });

    worker.on('failed', (job, error) => logger.error(`Job: ${job.name} failed. Error: ${error.message}`));

    worker.on('error', (err) => logger.error(err));

    return worker;
  });

  return workers;
};

export default initWorkers;
