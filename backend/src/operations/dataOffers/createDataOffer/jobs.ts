import { v4 as uuidv4 } from 'uuid';

import config from '../../../config';
import { DataOffer, DataPackage, DataType } from '../../../db/models';
import { pickRandomEntities } from '../../../utils/array';
import logger from '../../../utils/logger';
import { round } from '../../../utils/numbers';

import { addBatchIndividualTransaction } from '../../transactions';
import { getBuyerCredits, spendCredits } from '../../accounts/buyers';

import applyAudienceFilters from './applyAudienceFilters';
import { CreateOfferJobOpData, CreateOfferJobReturnValue } from './definitions';

export const initOffer = async (params: CreateOfferJobOpData): CreateOfferJobReturnValue => {
  const { submission } = params;
  const { offerBudget, companyId } = submission;

  logger.debug('Checking funds...');
  const credits = await getBuyerCredits(companyId);
  if (credits < offerBudget) {
    logger.error(`Company does not have enough credits: ${credits} < ${offerBudget}`);
    throw new Error(`Company does not have enough credits: ${credits} < ${offerBudget}`);
  }

  const offer: DataOffer = {
    ...submission,
    id: uuidv4(),
    numberOfUsers: 0,
    creditsSpent: 0,
    createdAt: Date.now(),
    status: 'initial',
  };

  return { offer };
};

export const getSizeOfAudience = async (
  params: CreateOfferJobOpData,
): CreateOfferJobReturnValue => {
  const { submission, services, jwtToken } = params;
  const { pricer } = services;
  const { audience, offerBudget, dataTypes } = submission;

  logger.debug('Getting size of audience...');
  const price = await pricer.getPrice(audience, dataTypes, jwtToken);
  const sizeOfAudience = Math.floor(offerBudget / price);

  if (logger.isDebugEnabled()) {
    logger.debug(`Price: ${price}`);
    logger.debug(`Size of Audience: ${sizeOfAudience}`);
  }
  return { price, sizeOfAudience };
};

export const getConsents = async (params: CreateOfferJobOpData): CreateOfferJobReturnValue => {
  const { submission, services } = params;
  const { consentManager } = services;
  const { dataTypes, purpose } = submission;

  logger.debug('Getting users with consents...');
  const usersIdsWithConsents = await consentManager.getUsersWithConsent(dataTypes, purpose);
  logger.debug(usersIdsWithConsents);

  return { usersIdsWithConsents };
};

export const getProfiles = async (params: CreateOfferJobOpData): CreateOfferJobReturnValue => {
  const { submission, services, values } = params;
  const { dataSafe } = services;
  const { audience } = submission;
  const { usersIdsWithConsents } = values;

  logger.debug('Getting profiles from Data Safe...');
  if (!usersIdsWithConsents) throw new Error('User with consents must be defined!');

  const profileData: Array<DataType> = ['personal-information'];
  if (audience.interest && audience.interest.length > 0) profileData.push('interests');

  const profiles = await dataSafe.getData(usersIdsWithConsents, profileData);
  logger.debug(profiles);
  return { profiles };
};

export const filterByAudience = async (params: CreateOfferJobOpData): CreateOfferJobReturnValue => {
  const { submission, values } = params;
  const { audience } = submission;
  const { profiles } = values;

  logger.debug('Filtering by audience...');
  if (!profiles) throw new Error('User profiles must be defined!');

  const filteredProfiles = await applyAudienceFilters(profiles, audience);
  logger.debug(filteredProfiles);
  return { profiles: filteredProfiles };
};

export const pickRandomProfiles = async (
  params: CreateOfferJobOpData,
): CreateOfferJobReturnValue => {
  const { profiles, sizeOfAudience } = params.values;

  logger.debug('Picking random profiles...');
  if (!profiles || !sizeOfAudience) throw new Error('User profiles and size of audience must be defined!');

  const pickedProfiles = pickRandomEntities(profiles, sizeOfAudience);
  logger.debug(pickedProfiles);
  return { profiles: pickedProfiles };
};

export const getDataPackages = async (params: CreateOfferJobOpData): CreateOfferJobReturnValue => {
  const { submission, services, values } = params;
  const { dataSafe } = services;
  const { dataTypes } = submission;
  const { profiles } = values;

  logger.debug('Getting data packages from Data Safe...');
  if (!profiles) throw new Error('User profiles must be defined!');

  const profilesIds = profiles.map((profile) => profile.id);
  const profilesData = await dataSafe.getData(profilesIds, dataTypes);
  const dataPackages: Array<DataPackage> = profilesData.map(({ id, ...data }) => data);
  return { dataPackages };
};

export const watermarkData = async (params: CreateOfferJobOpData): CreateOfferJobReturnValue => {
  const { services, values } = params;
  const { dataWatermarker } = services;
  const { dataPackages } = values;

  logger.debug('Watermarking data...');
  if (!dataPackages) throw new Error('Data Packages must be defined!');

  const wData = await dataWatermarker.watermarkData(dataPackages);
  logger.debug(wData);

  return { dataPackages: wData };
};

export const updateEndUsers = async (params: CreateOfferJobOpData): CreateOfferJobReturnValue => {
  const { services, values } = params;
  const { webhookClient } = services;
  const { offer, profiles } = values;

  logger.debug('Updating end users\' accounts...');
  if (!profiles || !offer) throw new Error('User Profiles and offer must be defined!');

  const profilesIds = profiles.map((profile) => profile.id);
  await addBatchIndividualTransaction(profilesIds, {
    type: 'dataOffer',
    createdAt: offer.createdAt,
    points: config.dataOffers.pointsPerUser,
    metadata: {
      offerId: offer.id,
      dataTypes: offer.dataTypes,
      purpose: offer.purpose,
    },
  }, webhookClient);
  return {};
};

export const updateDataBuyer = async (params: CreateOfferJobOpData): CreateOfferJobReturnValue => {
  const { submission, values } = params;
  const { companyId, offerBudget } = submission;
  const { profiles, price } = values;

  logger.debug('Updating data buyer account...');
  if (!profiles || !price) throw new Error('User Profiles and price must be defined!');

  const creditsSpent = Math.min(offerBudget, round(price * profiles.length, 2));
  if (logger.isDebugEnabled()) logger.debug(`Credits spent: ${creditsSpent}`);

  await spendCredits(companyId, creditsSpent);
  return { creditsSpent };
};

export const updateOffer = async (params: CreateOfferJobOpData): CreateOfferJobReturnValue => {
  const {
    offer, dataPackages, price, creditsSpent,
  } = params.values;

  logger.debug('Updating offer...');
  if (!dataPackages || !price || !offer || !creditsSpent) {
    throw new Error('Data Packages, price, creditsSpent and offer must be defined!');
  }

  const updatedOffer = {
    ...offer,
    numberOfUsers: dataPackages.length,
    creditsSpent,
    data: dataPackages,
  };

  return { offer: updatedOffer };
};
