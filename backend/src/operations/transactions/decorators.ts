import { Transaction } from '../../db/models';

const decorators: Record<Transaction['type'], (t: Transaction) => { description: string; }> = {
  dataOffer: (t: Transaction) => {
    if (t.type !== 'dataOffer') throw new Error('Not a data offer transaction');
    return {
      description: `Shared ${t.metadata.dataTypes.join(', ')} data for ${t.metadata.purpose}`,
    };
  },
  task: (t: Transaction) => {
    if (t.type !== 'task') throw new Error('Not a task transaction');
    return {
      description: `"${t.metadata.taskName}" task done`,
    };
  },
  other: (t: Transaction) => {
    if (t.type !== 'other') throw new Error('Not an "other" transaction');
    return {
      description: t.metadata.description,
    };
  },
};

export default decorators;
