import { v4 as uuidv4 } from 'uuid';
import format from 'date-fns/format';

import { individualUsersStore } from '../../db';
import { IndividualUser, Transaction, TransactionSubmission } from '../../db/models';
import { WebhookClient } from '../../services/definitions';

import { getOrCreateIndividualUser } from '../accounts/individuals';
import { callWebhooks } from '../webhooks';

import decorators from './decorators';

export type TransactionDTO = Omit<Transaction, 'createdAt'> & { description: string; date: string; };

export const getIndividualTransactions = async (userId: string): Promise<Array<TransactionDTO>> => {
  const user = await getOrCreateIndividualUser(userId);
  return user.history.transactions
    .sort((tA, tB) => tA.createdAt - tB.createdAt)
    .map(({ createdAt, ...t }) => ({
      ...t,
      ...decorators[t.type]({ createdAt, ...t }),
      date: format(createdAt, 'dd/MM/yyyy'),
    }));
};

const appendNewTransaction = (
  transactions: Array<Transaction>, transaction: TransactionSubmission,
): Array<Transaction> => [
  ...transactions,
  {
    ...transaction,
    id: uuidv4(),
  } as Transaction,
];

const notifyPointsChange = (client: WebhookClient, user: IndividualUser, change: number) => {
  callWebhooks(
    client,
    'end-users.points.change',
    {
      userId: user.id,
      change,
    },
  );
};

export const addIndividualTransaction = async (
  userId: string, transaction: TransactionSubmission, client: WebhookClient,
): Promise<void> => {
  const user = await getOrCreateIndividualUser(userId);
  const updatedUser = await individualUsersStore.upsert(
    userId,
    {
      history: {
        transactions: appendNewTransaction(user.history.transactions, transaction),
      },
    },
  );
  notifyPointsChange(client, updatedUser, transaction.points);
};

export const addBatchIndividualTransaction = async (
  userIds: Array<string>, transaction: TransactionSubmission, client: WebhookClient,
): Promise<void> => {
  const users = await Promise.all(userIds.map(getOrCreateIndividualUser));

  const updatedUsers = users.map(
    (user) => ({
      ...user,
      history: {
        ...user.history,
        transactions: appendNewTransaction(user.history.transactions, transaction),
      },
    }),
  );
  await individualUsersStore.batchStore(updatedUsers);
  updatedUsers.forEach((user) => notifyPointsChange(client, user, transaction.points));
};
