/* eslint-disable import/prefer-default-export */
import fs from 'fs';

import logger from '../utils/logger';
import { migrations as migrationsStore } from '../db';
import runTask from '../utils/runTask';

const executeMigration = async (filename: string) => {
  await runTask(filename, [], 'migrations');
  await migrationsStore.store({ id: filename, executed: true });
};

export const runMigrations = async () => {
  const executedMigrations = await migrationsStore.listKeys();
  const migrationList = fs.readdirSync(`${__dirname}/../migrations`);
  const newMigrations = migrationList
    .filter(
      (migration) => !migration.startsWith('index')
      && migration.match(/.*\.(ts|js)$/)
      && !executedMigrations.includes(migration.slice(0, -3)),
    ).sort();

  if (newMigrations.length === 0) {
    logger.info('No new migration found.');
    return;
  }
  logger.info(`${newMigrations.length} new migrations found. Starting execution`);
  // eslint-disable-next-line no-restricted-syntax
  for (const filename of newMigrations) {
    // eslint-disable-next-line no-await-in-loop
    await executeMigration(filename.slice(0, -3));
  }
  logger.info(`${newMigrations.length} migrations completed.`);
};
