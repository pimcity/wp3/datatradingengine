/* eslint-disable import/prefer-default-export */
import R from 'ramda';

import { companyUsersStore, dataOffersStore, individualUsersStore } from '../db';
import { DataOffer } from '../db/models';

const getOffersByData = (dataOffers: Array<DataOffer>) => dataOffers
  .map((dataOffer: DataOffer) => R.countBy((k: string) => k)(dataOffer.dataTypes))
  .reduce(
    (acc, dataTypesCount) => R.mergeWith(R.add, acc, dataTypesCount),
    {},
  );

export const getMetrics = async () => {
  const [
    dataBuyers,
    dataOffers,
    individuals,
  ] = await Promise.all([
    companyUsersStore.list(),
    dataOffersStore.list(),
    individualUsersStore.list(),
  ]);

  return {
    dataBuyers: dataBuyers.length,
    dataOffers: {
      ...getOffersByData(dataOffers),
      total: dataOffers.length,
    },
    individuals: individuals.length,
    totalPoints: R.sum(
      individuals.map(
        (i) => R.sum(i.history.transactions.filter((t) => t.type === 'dataOffer').map((t) => t.points)),
      ),
    ),
  };
};
