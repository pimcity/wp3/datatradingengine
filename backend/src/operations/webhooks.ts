/* eslint-disable import/prefer-default-export */
import { WebhookEventID } from '../types/Webhook';

import config from '../config';
import { WebhookClient } from '../services/definitions';
import logger from '../utils/logger';

const getWebHooks = (eventId: WebhookEventID) => {
  const { webhooks } = config.developers;
  return webhooks.filter((webhook) => webhook.eventId === eventId);
};

export const callWebhooks = async (
  client: WebhookClient, eventId: WebhookEventID, eventData: any,
) => {
  const webhooks = getWebHooks(eventId);

  await Promise.all(
    webhooks.map(
      ({ endpoint }) => client.callWebhook(endpoint, eventId, eventData)
        .catch((err) => {
          logger.error(`Error calling webhook ${endpoint}. ${err}`);
        }),
    ),
  );
};
