import { CompanyUser } from '../../db/models';
import { companyUsersStore } from '../../db';
import logger from '../../utils/logger';
import { round } from '../../utils/numbers';

export const addCredits = async (companyId: string, credits: number): Promise<CompanyUser> => {
  const company = await companyUsersStore.safeFetch(companyId);
  const roundedCredits = round(credits, 2);
  if (logger.isDebugEnabled()) logger.debug(company ? 'Incrementing credits' : 'Storing new company');
  return company
    ? companyUsersStore.incrementValues(company.id, { credits: roundedCredits })
    : companyUsersStore.store({ id: companyId, credits: roundedCredits, creditsSpent: 0 });
};

export const getBuyerCredits = async (userId: string): Promise<number> => {
  // FIXME: HACK
  const company = (await companyUsersStore.safeFetch(userId)) || (await addCredits(userId, 5000));
  logger.debug(company);
  return round(company.credits, 2);
};

export const getSpentCredits = async (userId: string): Promise<number> => {
  const company = await companyUsersStore.safeFetch(userId);
  return round(company?.creditsSpent || 0, 2);
};

export const spendCredits = async (companyId: string, credits: number): Promise<void> => {
  await companyUsersStore.incrementValues(companyId, {
    credits: -credits,
    creditsSpent: credits,
  });
};
