import startOfDay from 'date-fns/startOfDay';
import startOfWeek from 'date-fns/startOfWeek';
import startOfMonth from 'date-fns/startOfMonth';
import startOfYear from 'date-fns/startOfYear';
import { individualUsersStore } from '../../db';
import getIndividualUserPoints from './getIndividualUserPoints';

export const getOrCreateIndividualUser = async (
  id: string,
) => (await individualUsersStore.safeFetch(id)) || individualUsersStore.store({
  id,
  history: {
    transactions: [],
  },
});

export const getUserPoints = async (userId: string) => {
  const individual = await individualUsersStore.safeFetch(userId);
  const now = Date.now();
  return getIndividualUserPoints(individual, {
    allTime: [0, now],
    lastDay: [startOfDay(now).getTime(), now],
    lastWeek: [startOfWeek(now, { weekStartsOn: 1 }).getTime(), now],
    lastMonth: [startOfMonth(now).getTime(), now],
    lastYear: [startOfYear(now).getTime(), now],
  });
};

export const getUsersAboveThreshold = async (threshold: number, from: number, to: number) => {
  const individuals = await individualUsersStore.list();
  return individuals.map((i) => ({
    id: i.id,
    ...getIndividualUserPoints(i, { points: [from, to] }) as { points: number },
  })).filter(
    (i) => i.points >= threshold,
  ).map((i) => i.id);
};
