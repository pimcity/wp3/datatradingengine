import { IndividualUser, Transaction } from '../../db/models';
import { round } from '../../utils/numbers';

const getPoints = (transactions?: Array<Transaction>, from = 0, to = Date.now()) => {
  const points = transactions?.filter(
    (t) => t.createdAt >= from && t.createdAt <= to,
  ).reduce((acc, t) => acc + t.points, 0) || 0;
  return round(points, 2);
};

export type Intervals = Record<string, [number, number]>;

const getIndividualUserPoints = (
  individual?: IndividualUser, intervals: Intervals = {},
): Record<string, number> => Object.entries(intervals).reduce(
  (points, [key, [from, to]]) => ({
    ...points,
    [key]: getPoints(individual?.history.transactions, from, to),
  }),
  {},
);

export default getIndividualUserPoints;
