import fetch from 'node-fetch';

import logger from './logger';

export interface Authentication {
  token?: string;
  apiKey?: string;
}

type Authorization = { Authorization: string; } | { 'X-API-Key': string; };

const buildUrl = (baseUrl: string, path?: string) => {
  if (!path) return baseUrl;
  return `${baseUrl.replace(/\/$/, '')}/${path.replace(/^\//, '')}`;
};

const getAuthorization = (auth?: Authentication): Authorization | undefined => {
  if (auth?.token) return { Authorization: `Bearer ${auth?.token}` };
  if (auth?.apiKey) return { 'X-API-Key': auth.apiKey };
  return undefined;
};

export const get = async <R>(baseUrl: string, path?: string, auth?: Authentication): Promise<R> => {
  const res = await fetch(buildUrl(baseUrl, path), {
    method: 'GET',
    headers: {
      ...getAuthorization(auth),
      'Content-Type': 'application/json',
    },
  });
  return res.json();
};

export const post = async <T, R>(
  body: T, baseUrl: string, path?: string, auth?: Authentication,
): Promise<R> => {
  const url = buildUrl(baseUrl, path);
  const res = await fetch(
    url,
    {
      method: 'POST',
      headers: {
        ...getAuthorization(auth),
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body),
    },
  );
  if (res.status >= 400) {
    throw new Error(`POST to ${url} failed.${logger.isDebugEnabled() ? ` Body: ${JSON.stringify(body)}.` : ''} Response: ${await res.text()}`);
  }
  if (res.status === 204) return {} as R;
  return res.json();
};

export const postForm = async <T, R>(
  body: T, baseUrl: string, path?: string, auth?: Authentication,
): Promise<R> => {
  const formBody = Object.entries(body).map(([key, value]) => {
    const encodedKey = encodeURIComponent(key);
    const encodedValue = encodeURIComponent(value);
    return `${encodedKey}=${encodedValue}`;
  }).join('&');

  const res = await fetch(buildUrl(baseUrl, path), {
    method: 'POST',
    headers: {
      ...getAuthorization(auth),
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
    },
    body: formBody,
  });
  return res.json();
};
