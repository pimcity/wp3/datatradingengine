export default (value: any) => typeof value === 'object' && !Array.isArray(value);
