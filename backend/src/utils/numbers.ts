/* eslint-disable import/prefer-default-export */
export const round = (num: number, decimals: number) => Math.round(
  (num + Number.EPSILON) * (10 ** decimals),
) / (10 ** decimals);
