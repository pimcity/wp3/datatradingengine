/* eslint-disable import/prefer-default-export */

export const pickRandomEntities = <T>(entities: Array<T>, numberOfEntities: number): Array<T> => {
  if (entities.length <= numberOfEntities) return entities;

  let contestants = [...entities];
  const pickedEntities: Array<T> = [];
  for (let i = 0; i < numberOfEntities; i += 1) {
    const index = Math.round(Math.random() * (contestants.length - 1));
    pickedEntities.push(contestants[index]);
    contestants = [...contestants.slice(0, index), ...contestants.slice(index + 1)];
  }
  return pickedEntities;
};
