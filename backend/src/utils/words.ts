export const capitalize = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);
export const camelCase = (str: string) => str.charAt(0).toLowerCase() + str.slice(1);

export const titleize = (text: string) => text
  .replace(/_|-/g, ' ')
  .split(' ')
  .map(capitalize)
  .join(' ');

export const titleizeList = (list: Array<string>) => list
  .map(titleize)
  .join(', ')
  .replace(/, ([^,]*)$/, ' & $1');

export const listToCamelCase = (...words: Array<string>) => words.map(titleize).join('');

export const camelCaseKeys = (obj: { [key: string]: any }) => Object.keys(obj).reduce(
  (acc, key) => ({ ...acc, [camelCase(key)]: obj[key] }),
  {},
);

export const kebabToCamelCase = (str: string) => str.replace(/-([a-zA-Z])/g, (v) => v.slice(1).toUpperCase());

export const kebabToCamelCaseStrs = (strs: Array<string>) => strs.map(kebabToCamelCase);
