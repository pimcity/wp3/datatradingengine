/* eslint-disable global-require */
/* eslint-disable import/no-dynamic-require */
import path from 'path';
import glob from 'glob';

import logger from './logger';

const taskPathReplacer = (folder: string) => new RegExp(`^${__dirname}/../${folder}${path.sep}(.*)\\.js$`);

const getList = (folder: string) => glob
  .sync(`${__dirname}/../${folder}/**.{t,j}s`, { ignore: '**/index.{t,j}s' })
  .map((p: string) => {
    const tasks = require(p);
    const fileName = p.replace(taskPathReplacer(folder), '$1').split('/').pop() || '';
    const taskName = fileName.replace(/\.[jt]s/, '');
    if (tasks.default) {
      return taskName;
    }
    return Object.keys(tasks).map((t) => `${taskName}:${t}`);
  });

const tryRequire = (modulePath: string) => {
  try {
    return require(modulePath);
  } catch (_) {
    return {};
  }
};

const fail = (taskPath: string, folder: string) => {
  throw new Error(`Task '${taskPath}' does not exist.\nAvailable tasks:\n${getList(folder).join('\n')}`);
};

const runTask = async (taskPath: string, args: string[] = [], folder = 'migrations') => {
  if (!taskPath) {
    fail(taskPath, folder);
  }
  const taskDir = taskPath.split(':');
  taskDir.unshift(folder);
  taskDir.unshift('..');
  const taskName = taskDir.pop() || '';
  const modulePath = taskDir.join(path.sep);
  const task = tryRequire(modulePath + path.sep + taskName).default
    || tryRequire(modulePath)[taskName];
  if (!task) {
    fail(taskPath, folder);
  }
  logger.info(`Running task ${taskPath}...`);
  await task(...args);
  logger.info('Done');
};

export default runTask;
