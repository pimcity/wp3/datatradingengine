import {
  Audience, DataPackage, DataType, DataUser, Purpose,
} from '../db/models';
import { WebhookEventID } from '../types/Webhook';

export interface Pricer {
  getPrice(audience: Audience, dataTypes: Array<DataType>, authToken: string): Promise<number>;
}

export interface DataWatermarker {
  watermarkData(data: Array<DataPackage>): Promise<Array<DataPackage>>;
}

export interface ConsentManager {
  getUsersWithConsent(dataTypes: Array<DataType>, purpose: Purpose): Promise<Array<string>>;
}

export interface DataSafe {
  getDataMap(ids: Array<string>, dataTypes: Array<DataType>): Promise<Record<DataUser['id'], DataUser>>;
  getData(ids: Array<string>, dataTypes: Array<DataType>): Promise<Array<DataUser>>;
}

export interface TaskManager {
  registerTask(userIds: Array<string>, taskId: string): Promise<void>;
}

export interface WebhookClient {
  callWebhook(endpoint: string, eventId: WebhookEventID, eventData?: any): Promise<void>;
}

export interface Services {
  consentManager: ConsentManager;
  dataSafe: DataSafe;
  dataWatermarker: DataWatermarker;
  pricer: Pricer;
  taskManager: TaskManager;
  webhookClient: WebhookClient;
}
