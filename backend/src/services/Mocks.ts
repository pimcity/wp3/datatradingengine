import R from 'ramda';
import { DataUser } from '../db/models';

import {
  ConsentManager, DataSafe, DataWatermarker, Pricer,
} from './definitions';

class Mocks implements Pricer, ConsentManager, DataSafe, DataWatermarker {
  getPrice = async () => 10;

  getUsersWithConsent = async () => [
    '1111-1111-1111-1111',
    '2222-2222-2222-2222',
    '3333-3333-3333-3333',
    '4444-4444-4444-4444',
    '5555-5555-5555-5555',
  ];

  getData = async (ids: Array<string>): Promise<Array<DataUser>> => ids.map((id) => ({
    id,
    personalInformation: {
      gender: 'male',
    },
  }));

  getDataMap = async (ids: Array<string>): Promise<Record<DataUser['id'], DataUser>> => {
    const data = await this.getData(ids);
    return R.indexBy(R.prop('id'), data);
  }

  watermarkData = async (data: Array<any>) => data;
}

export default Mocks;
