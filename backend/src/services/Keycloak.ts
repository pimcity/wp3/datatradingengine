/* eslint-disable import/prefer-default-export */
import session from 'express-session';
import Keycloak from 'keycloak-connect';

import config from '../config';

let keycloakInstance: Keycloak.Keycloak;

const store = new session.MemoryStore();

export const getSessionStore = () => store;

export const getKeycloak = () => {
  if (!keycloakInstance) {
    keycloakInstance = new Keycloak(
      { store },
      {
        realm: config.security.keycloak.realm, // @ts-ignore
        realmPublicKey: config.security.keycloak.realmPublicKey,
        serverUrl: config.security.keycloak.serverUrl,
        clientId: config.security.keycloak.clientId,
        bearerOnly: true,
      },
    );
  }
  return keycloakInstance;
};

export const protect = (
  keycloak: Keycloak.Keycloak,
  roles: Array<string>,
) => keycloak.protect((token) => !!roles.find((role) => token.hasRole(role)));
