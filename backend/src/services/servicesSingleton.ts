import { Services } from './definitions';
import DataProvenance from './implementations/DataProvenance';
import DVTMP from './implementations/DVTMP';
import MultiDataSafe from './implementations/MultiDataSafe';
import PCM from './implementations/PCM';
import PDS from './implementations/PDS';
import TM from './implementations/TM';
import UP from './implementations/UP';
import WebhookClientImpl from './implementations/WebhookClientImpl';

const createMultiDataSafe = () => {
  const pds = new PDS();
  const up = new UP(pds.getToken());
  return new MultiDataSafe([pds, up]);
};

class ServicesSingleton {
  services?: Services;

  getServices = (): Services => {
    if (!this.services) {
      this.services = {
        consentManager: new PCM(),
        dataSafe: createMultiDataSafe(),
        dataWatermarker: new DataProvenance(),
        pricer: new DVTMP(),
        taskManager: new TM(),
        webhookClient: new WebhookClientImpl(),
      };
    }

    return this.services;
  }
}

const servicesSingleton = new ServicesSingleton();

export default servicesSingleton;
