import { WebhookEventID } from '../../types/Webhook';
import { post } from '../../utils/http';

import { WebhookClient } from '../definitions';

class WebhookClientImpl implements WebhookClient {
  callWebhook = (
    endpoint: string, eventId: WebhookEventID, eventData?: any,
  ): Promise<void> => post(
    {
      eventId,
      eventData,
    },
    endpoint,
  );
}

export default WebhookClientImpl;
