import R from 'ramda';
import { DataType, DataUser } from '../../db/models';

import { DataSafe } from '../definitions';

class MultiDataSafe implements DataSafe {
  dataSafes: Array<DataSafe>;

  constructor(dataSafes: Array<DataSafe>) {
    this.dataSafes = dataSafes;
  }

  getDataMap = async (ids: Array<string>, dataTypes: Array<DataType>): Promise<Record<DataUser['id'], DataUser>> => {
    const dataMaps = await Promise.all(
      this.dataSafes.map((dataSafe) => dataSafe.getDataMap(ids, dataTypes)),
    );
    return R.reduce<Record<DataUser['id'], DataUser>, Record<DataUser['id'], DataUser>>(R.mergeDeepLeft, {}, dataMaps);
  };

  getData = async (ids: Array<string>, dataTypes: Array<DataType>): Promise<Array<DataUser>> => {
    const dataUsers = await this.getDataMap(ids, dataTypes);
    return ids.map((id) => dataUsers[id]);
  };
}

export default MultiDataSafe;
