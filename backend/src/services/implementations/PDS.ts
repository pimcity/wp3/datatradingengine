import R from 'ramda';

import config from '../../config';
import countryCodes from '../../db/countryCodes.json';
import educations from '../../db/educations.json';
import jobs from '../../db/jobs.json';
import {
  ContactInformation, DataPackage, DataType, DataUser, SocioDemographics,
} from '../../db/models';
import { post, postForm } from '../../utils/http';
import { kebabToCamelCase } from '../../utils/words';

import { DataSafe } from '../definitions';

// eslint-disable-next-line camelcase
const getToken = () => postForm<unknown, { access_token: string }>(
  {
    grant_type: 'client_credentials',
    client_id: config.services.PDS.clientId,
    client_secret: config.services.PDS.clientSecret,
  },
  config.security.keycloak.serverUrl,
  '/realms/pimcity/protocol/openid-connect/token',
).then((res) => res.access_token);

const dataDictionary: Record<DataType, Array<string>> = {
  'contact-information': ['first-name', 'last-name', 'telephone'],
  'personal-information': ['birth-date', 'gender', 'country', 'household-members', 'job', 'income', 'education'],
  'browsing-history': ['visited-url'],
  'location-history': ['visited-location'],
  'social-networks-data': [],
  'financial-data': [],
  interests: [],
};

const dataTypeToDataGroup = (dataType: DataType) => {
  if (dataType === 'contact-information') return 'personal-information';
  return dataType;
};

const dataTypeToDataUser = (dataType: DataType): keyof DataPackage => {
  if (dataType === 'personal-information') return 'socioDemographics';
  return kebabToCamelCase(dataType) as keyof DataPackage;
};

interface DataPiece {
  'group_name': string;
  value: {
    [dataField: string]: string;
  };
}

type PDSResponse = Record<DataUser['id'], Array<DataPiece>>;

interface PDSContextualResponse {
  dataType: DataType;
  dataField: string;
  data: PDSResponse;
}

const getDataPiece = async (
  ids: Array<string>, dataType: DataType, dataField: string, token?: string,
) => {
  const data = await post<unknown, PDSResponse>(
    {
      users: ids,
      'data-group': dataTypeToDataGroup(dataType),
      'data-type': dataField,
    },
    config.services.PDS.url,
    '/data-buyers/get-data/',
    { token },
  );
  return { dataType, dataField, data };
};

type FieldKey = keyof SocioDemographics | keyof ContactInformation;

const formatters: Record<FieldKey, (value: string) => any> = {
  firstName: (v: string) => v,
  lastName: (v: string) => v,
  telephone: (v: string) => v,
  gender: (v: string) => v.toLowerCase(),
  country: (v: string) => countryCodes.find((c) => c.Name === v)?.Code,
  birthDate: (v: string) => v,
  income: (v: string) => Number(v),
  householdMembers: (v: string) => Number(v),
  education: (v: string) => educations.find((e) => e.label === v)?.id,
  job: (v: string) => jobs.find((e) => e.label === v)?.id,
};

const toFieldMap = (
  dataField: string, dataPieces: Array<DataPiece>,
): DataUser['socioDemographics'] | DataUser['contactInformation'] => {
  const camelDataField = kebabToCamelCase(dataField) as FieldKey;
  const formatter = formatters[camelDataField];
  return {
    [camelDataField]: dataPieces.length > 0 ? formatter(dataPieces[0].value[dataField]) : undefined,
  };
};

const mapDataToDataUser = (
  dataType: DataType, dataField: string, dataPieces: Array<DataPiece>,
): DataPackage[keyof DataPackage] => {
  if (dataType === 'personal-information' || dataType === 'contact-information') return toFieldMap(dataField, dataPieces);
  return dataPieces as Array<any>;
};

const responseToDataUsers = (response: PDSContextualResponse): Record<DataUser['id'], DataUser> => {
  const dataUsers: Record<DataUser['id'], DataUser> = R.mapObjIndexed((dataPieces, userId) => ({
    id: userId,
    [dataTypeToDataUser(response.dataType)]: mapDataToDataUser(
      response.dataType, response.dataField, dataPieces,
    ),
  }), response.data);
  return dataUsers;
};

const toDataUsers = (responses: Array<PDSContextualResponse>): Record<DataUser['id'], DataUser> => R.pipe(
  R.map(responseToDataUsers),
  R.reduce<Record<DataUser['id'], DataUser>, Record<DataUser['id'], DataUser>>(R.mergeDeepLeft, {}),
)(responses);

class PDS implements DataSafe {
  token?: string;

  getToken = async (): Promise<string> => {
    if (!this.token) this.token = await getToken();
    return this.token;
  }

  getDataMap = async (ids: Array<string>, dataTypes: Array<DataType>): Promise<Record<DataUser['id'], DataUser>> => {
    const token = await this.getToken();

    const allProms = dataTypes.map((dataType) => {
      const fields = dataDictionary[dataType];
      return fields.map((field) => getDataPiece(ids, dataType, field, token));
    });
    const responses = await Promise.all(R.flatten(allProms));
    return toDataUsers(responses);
  };

  getData = async (ids: Array<string>, dataTypes: Array<DataType>): Promise<Array<DataUser>> => {
    const dataUsers = await this.getDataMap(ids, dataTypes);
    return ids.map((id) => dataUsers[id]);
  };
}

export default PDS;
