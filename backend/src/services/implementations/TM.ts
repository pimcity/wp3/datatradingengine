import config from '../../config';
import { post } from '../../utils/http';
import logger from '../../utils/logger';

import { TaskManager } from '../definitions';

class TM implements TaskManager {
  registerTask = async (userIds: Array<string>, taskId: string): Promise<void> => {
    logger.debug('register task start');
    await post(
      {
        taskId,
        userIds,
      },
      config.services.TM.url,
      '/tasks',
      { apiKey: config.services.TM.apiKey },
    );
    logger.debug('register task done');
  }
}

export default TM;
