import R from 'ramda';

import config from '../../config';
import { DataType, DataUser, Interest } from '../../db/models';
import { post } from '../../utils/http';

import { DataSafe } from '../definitions';

interface UPInterestCategory {
  name: string;
  relevance: number;
}

interface UPInterest extends UPInterestCategory {
  subCategories: Array<UPInterestCategory>;
}

interface UPUser {
  user: string;
  profile: Array<UPInterest>;
}

const getProfiles = async (ids: Array<string>, token: string) => post<unknown, Array<UPUser>>(
  ids,
  config.services.UP.url,
  '/getProfile',
  { token },
);

const toInterests = (profile: Array<UPInterest>): Array<Interest> => {
  const allInterests = profile.reduce(
    (interests, interest) => [...interests, interest, ...interest.subCategories],
    [] as Array<UPInterestCategory>,
  );
  return allInterests.filter((i) => i.relevance > 0).map((i) => i.name as Interest);
};

const toDataUsers = (profiles: Array<UPUser>): Array<DataUser> => profiles.map(
  ({ user, profile }) => ({
    id: user,
    interests: toInterests(profile),
  }),
);

class UP implements DataSafe {
  tokenProm: Promise<string>;

  constructor(pdsTokenPromise: Promise<string>) {
    this.tokenProm = pdsTokenPromise;
  }

  getData = async (ids: Array<string>, dataTypes: Array<DataType>): Promise<Array<DataUser>> => {
    if (!dataTypes.includes('interests')) return [];
    const token = await this.tokenProm;
    const profiles = await getProfiles(ids, token);
    return toDataUsers(profiles);
  };

  getDataMap = async (ids: Array<string>, dataTypes: Array<DataType>): Promise<Record<DataUser['id'], DataUser>> => {
    const data = await this.getData(ids, dataTypes);
    return R.indexBy(R.prop('id'), data);
  }
}

export default UP;
