import { DataPackage } from '../../db/models';

import { DataWatermarker } from '../definitions';

class DataProvenance implements DataWatermarker {
  watermarkData = async (data: Array<DataPackage>) => data;
}

export default DataProvenance;
