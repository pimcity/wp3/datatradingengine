import config from '../../config';
import {
  Audience, Country, DataType, Gender, Interest,
} from '../../db/models';
import { post } from '../../utils/http';
import { round } from '../../utils/numbers';

import { Pricer } from '../definitions';

const DEFAULT_COUNTRY: Country = 'US';

const MIN_AGE = 13;
const MAX_AGE = 65;

const IAB_TO_FB = {
  IAB1: 'Fine art',
  IAB2: 'Automotive industry',
  IAB3: 'Business',
  IAB4: 'Career',
  IAB5: 'Education',
  IAB6: 'Family and relationships',
  IAB7: 'Health care',
  IAB8: 'Food and drink',
  IAB9: 'Hobbies and activities',
  IAB10: 'Home and garden',
  IAB11: 'Law',
  IAB12: 'Local news',
  IAB13: 'Personal finance',
  IAB14: 'Politics and social issues',
  IAB15: 'Medicine',
  IAB16: 'Pets',
  IAB17: 'Sports and outdoors',
  IAB18: 'Fashion design',
  IAB19: 'Computer network',
  IAB20: 'Travel',
  IAB21: 'Real estate',
  IAB22: 'Shopping and fashion',
  IAB23: 'Religion',
  IAB24: undefined,
  IAB25: undefined,
  IAB26: undefined,
};

const countryAdapter = (
  countries?: Array<Country>,
): Array<string> => (countries && countries.length > 0 ? countries : [DEFAULT_COUNTRY]);

const iabToFbAdapter = (interests?: Array<Interest>) => (interests && interests.length > 0
  ? IAB_TO_FB[interests[0]] : [IAB_TO_FB.IAB21]);

const genderAdapter = (genders?: Array<Gender>) => {
  if (!genders || genders.length === 0) return 'all';
  const gender = genders[0];
  return gender === 'other' ? 'all' : gender;
};

interface DVTMPResponse {
  data: {
    value: number;
  }
}

class DVTMP implements Pricer {
  getPrice = async (
    audience: Audience, dataTypes: Array<DataType>, authToken: string,
  ): Promise<number> => {
    const { data: { value } } = await post<unknown, DVTMPResponse>(
      {
        gender: genderAdapter(audience.gender),
        location: countryAdapter(audience.country),
        interest: iabToFbAdapter(audience.interest),
        min_age: Math.max(audience.age?.min || MIN_AGE, MIN_AGE),
        max_age: Math.min(audience.age?.max || MAX_AGE, MAX_AGE),
        platform: 'linkedIn',
        priceType: 'CPM',
      },
      config.services.DVTMP.url,
      '/auValuator/',
      { token: authToken },
    );
    const dataPieces = dataTypes.length;
    const roundedPrice = round((dataPieces * Math.abs(value)) / 1000, 2);
    return Math.max(roundedPrice, 0.01);
  }
}

export default DVTMP;
