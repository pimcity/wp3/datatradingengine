import config from '../../config';
import { DataType, Purpose } from '../../db/models';
import { post } from '../../utils/http';

import { ConsentManager } from '../definitions';

class PCM implements ConsentManager {
  getUsersWithConsent = async (
    dataTypes: Array<DataType>, purpose: Purpose,
  ): Promise<Array<string>> => {
    const { users } = await post<unknown, { users: any }>(
      {
        dataTypes,
        purpose,
      },
      config.services.PCM.url,
      '/certificates/data-sharing',
      { apiKey: config.services.PCM.apiKey },
    );
    return users;
  }
}

export default PCM;
