import Router from 'express-promise-router';
import { CompanyInformation } from '../db/models';
import { runSafe } from '../middlewares/run';
import { companiesInformationStore } from '../db';
import { AuthRequest } from '../types/middlewares/auth';

const router = Router();

router.get('/', runSafe(() => companiesInformationStore.list()));

router.post('/', runSafe(
  (req: AuthRequest) => {
    const company: CompanyInformation = req.body;
    return companiesInformationStore.store({
      id: req.user.id,
      companyDomain: company.companyDomain,
      companyName: company.companyName,
      contactEmail: company.contactEmail,
      privacyPolicyUrl: company.privacyPolicyUrl,
    });
  },
));

export default router;
