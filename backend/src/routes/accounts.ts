import Router from 'express-promise-router';
import { Request } from 'express';

import decorateUserRequest from '../middlewares/decorateUserRequest';
import validateInternalAuthentication from '../middlewares/internalAuth';
import { runSafe } from '../middlewares/run';

import { getUserPoints, getUsersAboveThreshold } from '../operations/accounts/individuals';
import { addCredits, getBuyerCredits, getSpentCredits } from '../operations/accounts/buyers';

import { getKeycloak, protect } from '../services/Keycloak';
import { AuthRequest } from '../types/middlewares/auth';

const keycloak = getKeycloak();

const router = Router();
router.use(keycloak.middleware());

router.get(
  '/end-users/points',
  protect(keycloak, ['end-user']),
  decorateUserRequest,
  runSafe((req: AuthRequest) => getUserPoints(req.user.id)),
);

router.get(
  '/end-users/points/:userId',
  validateInternalAuthentication,
  runSafe((req: AuthRequest) => getUserPoints(req.params.userId)),
);

router.get(
  '/end-users/points/list/:threshold/:from/:to',
  validateInternalAuthentication,
  runSafe(
    (req: Request) => getUsersAboveThreshold(
      Number(req.params.threshold),
      Number(req.params.from),
      Number(req.params.to),
    ),
  ),
);

router.get(
  '/buyers/credits',
  protect(keycloak, ['data-buyer']),
  decorateUserRequest,
  runSafe((req: AuthRequest) => getBuyerCredits(req.user.id)),
);

router.post(
  '/buyers/credits',
  validateInternalAuthentication,
  runSafe((req: Request) => addCredits(req.body.userId, req.body.credits)),
);

router.get(
  '/buyers/credits/spent',
  keycloak.protect('data-buyer'),
  decorateUserRequest,
  runSafe((req: AuthRequest) => getSpentCredits(req.user.id)),
);

export default router;
