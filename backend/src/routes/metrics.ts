import Router from 'express-promise-router';
import { Request } from 'express';

import { runSafe } from '../middlewares/run';
import { getMetrics } from '../operations/metrics';
import { getKeycloak, protect } from '../services/Keycloak';

const keycloak = getKeycloak();

const router = Router();
router.use(keycloak.middleware());

router.get(
  '/',
  protect(keycloak, ['admin']),
  runSafe((req: Request) => getMetrics()),
);

export default router;
