import Router from 'express-promise-router';
import { Request, Response } from 'express';
import config from '../config';

const router = Router();

router.get('/', (req: Request, res: Response) => {
  res.json({ status: 'OK', app: config.app });
});

export default router;
