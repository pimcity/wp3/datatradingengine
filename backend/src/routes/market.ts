import { Request } from 'express';
import Router from 'express-promise-router';
import type QueryString from 'qs';

import { Audience, DataType, OfferStatus } from '../db/models';
import { runSafe } from '../middlewares/run';
import decorateUserRequest from '../middlewares/decorateUserRequest';
import validateInternalAuthentication from '../middlewares/internalAuth';

import {
  getCompanyDataOffers,
  getIndividualDataOffers,
  getIndividualDataOffersMissed,
  getCompanyDataOfferData,
} from '../operations/dataOffers/getDataOffers';
import createDataOffer from '../operations/dataOffers/createDataOffer';
import { addIndividualTransaction, getIndividualTransactions } from '../operations/transactions';

import servicesSingleton from '../services/servicesSingleton';
import { getKeycloak } from '../services/Keycloak';

import { AuthRequest } from '../types/middlewares/auth';
import { BadRequestError } from '../utils/error';

const keycloak = getKeycloak();

const services = servicesSingleton.getServices();

const router = Router();

const getQueryValue = <T =unknown>(query: QueryString.ParsedQs, name: string): T | undefined => {
  const valueStr = query ? query[name] : undefined;
  return valueStr && typeof valueStr === 'string' ? JSON.parse(valueStr) : undefined;
};

router.get(
  '/price',
  keycloak.protect('data-buyer'),
  decorateUserRequest,
  runSafe(
    async (req: AuthRequest) => {
      const audience = getQueryValue<Audience>(req.query, 'audience');
      const dataTypes = getQueryValue<Array<DataType>>(req.query, 'dataTypes');
      if (!audience || !dataTypes) throw new BadRequestError('Missing audience or data types');
      return {
        pricePerIndividual: await services.pricer.getPrice(audience, dataTypes, req.user.token),
      };
    },
  ),
);

router.get(
  '/end-users/data-offers',
  keycloak.protect('end-user'),
  decorateUserRequest,
  runSafe(
    (req: AuthRequest) => getIndividualDataOffers(req.user.id),
  ),
);

router.get(
  '/end-users/data-offers/missed',
  keycloak.protect('end-user'),
  decorateUserRequest,
  runSafe(
    (req: AuthRequest) => getIndividualDataOffersMissed(req.user.id),
  ),
);

router.get(
  '/end-users/transactions',
  keycloak.protect('end-user'),
  decorateUserRequest,
  runSafe(
    (req: AuthRequest) => getIndividualTransactions(req.user.id),
  ),
);

router.post(
  '/end-users/transactions/:userId/task',
  validateInternalAuthentication,
  runSafe(
    (req: Request) => addIndividualTransaction(
      req.params.userId,
      {
        points: req.body.points,
        createdAt: req.body.createdAt,
        type: 'task',
        metadata: {
          taskId: req.body.taskId,
          taskName: req.body.taskName,
        },
      },
      services.webhookClient,
    ),
  ),
);

router.post(
  '/end-users/transactions/:userId/other',
  validateInternalAuthentication,
  runSafe(
    (req: Request) => addIndividualTransaction(
      req.params.userId,
      {
        points: req.body.points,
        createdAt: req.body.createdAt,
        type: 'other',
        metadata: {
          description: req.body.description,
        },
      },
      services.webhookClient,
    ),
  ),
);

router.post(
  '/end-users/transactions/other',
  keycloak.protect('admin'),
  runSafe(
    (req: Request) => {
      const proms = req.body.users.map((userId: string) => addIndividualTransaction(
        userId,
        {
          points: req.body.points,
          createdAt: req.body.createdAt,
          type: 'other',
          metadata: {
            description: req.body.description,
          },
        },
        services.webhookClient,
      ));
      return Promise.all(proms);
    },
  ),
);

const toOfferStatus = (
  param: string | QueryString.ParsedQs | string[] | QueryString.ParsedQs[] | undefined,
): Array<OfferStatus> | undefined => (typeof param === 'string' ? param.split(',') : param) as Array<OfferStatus> | undefined;

router.get(
  '/buyers/data-offers',
  keycloak.protect('data-buyer'),
  decorateUserRequest,
  runSafe((req: AuthRequest) => getCompanyDataOffers(
    req.user.id,
    {
      include: toOfferStatus(req.query.include),
      exclude: toOfferStatus(req.query.exclude),
    },
  )),
);

router.get(
  '/buyers/data-offers/:offerId/data',
  keycloak.protect('data-buyer'),
  decorateUserRequest,
  runSafe((req: AuthRequest) => getCompanyDataOfferData(req.user.id, req.params.offerId)),
);

router.post(
  '/buyers/data-offers',
  keycloak.protect('data-buyer'),
  decorateUserRequest,
  runSafe(
    (req: AuthRequest) => createDataOffer(
      {
        ...req.body,
        companyId: req.user.id,
      },
      req.user.token,
    ),
  ),
);

export default router;
