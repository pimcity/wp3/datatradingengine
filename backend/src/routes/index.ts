import Router from 'express-promise-router';
import session from 'express-session';

import { getKeycloak, getSessionStore } from '../services/Keycloak';

import health from './health';
import accounts from './accounts';
import companies from './companies';
import market from './market';
import metrics from './metrics';

const keycloak = getKeycloak();
const store = getSessionStore();

const router = Router();

router.use(session({
  secret: 'some secret',
  resave: false,
  saveUninitialized: true,
  store,
}));
router.use(keycloak.middleware());

router.use('/health', health);
router.use('/metrics', metrics);
router.use('/data-buyers', companies);
router.use('/accounts', accounts);
router.use('/market', market);

export default router;
