import { Request, Response, NextFunction } from 'express';

import { AuthRequest } from '../types/middlewares/auth';
import { UnauthorizedError } from '../utils/error';

const decorateUserRequest = (req: Request, res: Response, next: NextFunction) => {
  if (!req.headers.authorization) {
    throw new UnauthorizedError('Missing Authorization header');
  }

  const decoratedReq = req as AuthRequest;
  decoratedReq.user = { // @ts-ignore
    id: req.kauth.grant.access_token.content.sub,
    token: req.headers.authorization.replace('Bearer ', ''),
  };
  next();
};

export default decorateUserRequest;
