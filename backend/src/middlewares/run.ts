/* eslint-disable import/prefer-default-export */
import { Request, Response, NextFunction } from 'express';
import { ApiError } from '../utils/error';

interface SafeOptions {
  status?: number;
  emptyStatus?: number;
  ErrorType?: typeof ApiError;
  silent?: boolean;
}

interface Options extends SafeOptions {
  params?: Function;
}

export const runSafe = <T extends Request>(
  handler: (req: T, res: Response) => any,
  options: SafeOptions = {},
) => async (
    req: Request,
    res: Response,
    next: NextFunction,
  ) => {
    const {
      status = 200,
      emptyStatus = 204,
      ErrorType = ApiError,
      silent = false,
    } = options;

    try {
      const response = await handler(req as T, res);
      const responseStatus = response !== undefined && response !== null ? status : emptyStatus;
      res.status(responseStatus).json(response);
    } catch (error) {
      next(
        !(error as any).send && error instanceof Error
          ? new ErrorType(error.message, silent)
          : error,
      );
    }
  };

const getBody = (req: Request) => [req.body];

/**
 * @deprecated
 * @function run Invokes operation and responds its result
 * @param {function} operation Function that runs the bussines logic
 * @param {Options} options
 */
export const run = (
  operation: Function, options: Options = {},
) => runSafe(
  (req) => {
    const { params = getBody } = options;
    return operation(...params(req));
  },
  options,
);
