import { Route } from 'react-router-dom';

import Welcome from './screens/Welcome';

function App() {
  return (
    <div className="w-screen h-screen">
      <Route exact path="/" component={Welcome} />
    </div >
  );
}

export default App;
