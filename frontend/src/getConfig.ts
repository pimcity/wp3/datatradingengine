interface ProcessEnv {
  [key: string]: string | undefined
}

/**
 * `process` is not defined until you are inside a React Component.
 * CALL THIS ONLY FROM WITHIN A REACT COMPONENT
 */
const getConfig = (env: ProcessEnv) => ({
  env: env.NODE_ENV,
  apiUrl: env.REACT_APP_API_URL,
  publicUrl: env.PUBLIC_URL as string,
  i18nDfltLng: env.REACT_APP_REGION_DFLT_LANG || 'en',
});

export default getConfig;
