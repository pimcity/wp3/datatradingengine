import { Link } from 'react-router-dom';

const themeClasses = {
  primary: 'bg-primary-600 hover:bg-primary-700 focus:ring-primary-600',
  secondary: 'bg-secondary-600 hover:bg-secondary-700 focus:ring-secondary-600',
};

const ButtonLink = ({
  to,
  theme = 'primary',
  children,
}: { to: string, theme?: 'primary' | 'secondary', children: any }) => (
  <Link
    className={`inline-flex items-center px-3 py-3 font-medium text-white border border-transparent shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 transition duration-300 ease-in-out leading-4 rounded-md ${themeClasses[theme]}`}
    to={to}
  >
    {children}
  </Link>
);

export default ButtonLink;
