const themeClasses = {
  primary: 'bg-primary-600 hover:bg-primary-700 focus:ring-primary-600',
  secondary: 'bg-secondary-600 hover:bg-secondary-700 focus:ring-secondary-600',
};

const ExternalLink = ({
  href,
  theme = 'primary',
  children,
}: { href: string, theme?: 'primary' | 'secondary', children: any }) => (
  <a
    className={`inline-flex items-center px-3 py-3 font-medium text-white border border-transparent shadow-sm focus:outline-none focus:ring-2 focus:ring-offset-2 transition duration-300 ease-in-out leading-4 rounded-md ${themeClasses[theme]}`}
    href={href}
    target="_blank"
    rel="noreferrer"
  >
    {children}
  </a>
);

export default ExternalLink;
