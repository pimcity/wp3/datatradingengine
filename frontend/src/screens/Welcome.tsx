import styles from './Welcome.module.css';

import getConfig from '../getConfig';
import ExternalLink from '../components/ExternalLink';

const Welcome = () => (
  <div className={styles.welcome}>
    <nav className="flex items-center px-20 pt-4 pb-6 bg-primary-600">
      <img className="w-24 p-1 bg-white rounded" src={`${getConfig(process.env).publicUrl}/images/pimcity_logo.png`} />
      <div className="flex-1 text-center">
        <h1 className="mb-1">PIMCity Demonstration</h1>
        <h4>BUILDING THE NEXT GENERATION PERSONAL DATA PLATFORMS</h4>
      </div>
    </nav>
    <div className="px-20 pt-12 pb-6">
      <div className={styles.card}>
        <header>
          <h2>Data Trading Engine</h2>
        </header>
        <section>
          <div className="flex justify-between mb-4">
            <img className="h-auto w-80" src={`${getConfig(process.env).publicUrl}/images/dte.png`} />
            <div>
              <p>
                The <strong>Data Trading Engine (D-TE)</strong> is responsible for trading
                the data of users registered/handled by the PIM with interested buyers.
                Hence, the D-TE serves as a communication interface between the PIM backend
                and the data buyers. There is a myriad of data types that can be sold. The D-TE
                focuses in bulk data and audience data.
              </p>
              <p>
                Its primary objective is to execute all transactions within the platform to
                exchange data for value in a secure, transparent and fair-for-all way. Moreover,
                its key requirement is to be fully GDPR compliant. That is, it must be able to
                receive Data Offers from Data Buyers and fulfil them with users from the PIMCity
                platform that not only fit within the target data sought, but also that have
                proactively consented to share that data with that company for a specific purpose.
              </p>
            </div>
          </div>
          <div className="flex justify-between mb-4">
            <div className="w-1/3 mr-3">
              <h3>Access the API</h3>
              <p>See the OpenAPI definition of the Data Trading Engine.</p>
              <ExternalLink theme="secondary" href="https://easypims.pimcity-h2020.eu/dte-api/api-docs/">
                Go to the online Definition
              </ExternalLink>
            </div>
            <div className="w-1/3 mr-3">
              <h3>Source Code</h3>
              <p>The project is open-source and its code is on the online repository:</p>
              <ExternalLink theme="secondary" href="https://gitlab.com/pimcity/wp3/datatradingengine">GitLab</ExternalLink>
            </div>
            <div className="w-1/3">
              <h3>Licence</h3>
              <p>
                The D-TE is distributed under AGPL-3.0-only, see the LICENSE file in the project
                repository.
                Copyright (C) 2021  Wibson - Daniel Fernandez, Rodrigo Irarrazaval
              </p>
            </div>
          </div>
        </section>
        <footer>
          This project has received funding from the European Union Horizon 2020
          Research and Innovation programme under{' '}
          <a href="https://cordis.europa.eu/project/rcn/225755/factsheet/en">Grant Agreement No. 871370</a>
        </footer>
      </div>
    </div>
  </div >
);

export default Welcome;
