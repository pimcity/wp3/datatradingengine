#!/bin/sh

NODE_ENV=development yarn

## wait for deps
echo "Waiting DEPS..."
while ! nc -z "$DEP_HOST" "$DEP_PORT"; do sleep 5; done
echo "DEPS OK"

if [ $NODE_ENV = 'production' ]
then
  yarn build
  yarn serve
else
  if [ $DEBUG_MODE ]
  then
    yarn debug
  else
    yarn start
  fi
fi