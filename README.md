Data Trading Engine
===================

# Introduction

The Data Trading Engine (DTE) is responsible for trading the data of users registered/handled by the PIM with interested buyers. Hence, the DTE serves as a communication interface between the PIM backend and the data buyers. There is a myriad of data types that can be sold. The DTE focuses in bulk data and audience data.

Bulk data is typically bought in a non-real-time manner to receive information from a (typically large) group of users. Some examples include: a health insurance company may be interested in people's medical records; a car insurance company may be interested in the people's mobility data to understand who drives through tough roads or at higher speeds; a mortgage-issuing company may be interested in people's financial records, etc. None of these data need to be traded in real-time, and typically data buyers try to buy it in bulk.

An audience is a term used in marketing to refer to a specific group of the population defined by three parameters: location (where the user is located); demographic information (age, gender, etc.); interests (a list of interests, e.g., outdoor activities, sports, science, automobile, etc.). Most of these parameters are typically extracted from well-known taxonomies such as the one offered by IAB (which is a de-facto standard in digital marketing). Online advertising (a.k.a. digital marketing) is arguably one of the most important businesses exploiting data utilization, specifically audience data, to deliver targeted ads to users in real-time.

The DTE is exposed as a REST API which can be accessed by other components in PIMCity or external players like Data Buyers and individuals. The creation of a transaction is depicted in the figure below.

![image info](dte.png)

# Installation

The documentation and the following instructions refer to a Linux environment, running with **Docker Engine v20.10.x** and **Docker Compose: v1.27.x**. The DTE project has been cloned from [this GitLab repository](https://gitlab.com/pimcity/wp3/datatradingengine.git).

Follow accurately the next steps to quickly set-up the DTE backbone on your server. All relevant steps are designed for a Linux machine, perform the equivalent procedure with other environments.

1. Prepare the environment:
```
> sudo apt-get update
> sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
> curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
> echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
> sudo apt-get update
> sudo apt-get install docker-ce docker-ce-cli containerd.io
```

2. Import the project from the GIT repository:
```
> git clone https://gitlab.com/pimcity/wp3/datatradingengine.git
```

# Usage

## Execution
To run the Trading Engine service, the following command should be executed at the root of the repository: 
```
> docker-compose up
```

## Configuration
The Trading Engine can be configured in three similar ways: 

1. Using environment variables. 
2. Defining those environment variables in a file called “.env” at the root directory of the folder with the PDK deployed, declaring them in the same way a variable is defined in the UNIX shell. You can check [this example](.env.example).
3. Modifying [docker-compose.yml](docker-compose.yml) file.

## Usage Examples
You can check [here](https://easypims.pimcity-h2020.eu/dte-api/api-docs/) the Swagger OpenAPI definition to see how the API is defined and used. Examples are provided as well.

# Changes

## Additions

- An endpoint to log transactions and credits that were originated in other PDKs. It requires an API Key.
- An endpoint to read transactions that were originated in other PDKs. It requires an API Key.

# License

The DTE is distributed under AGPL-3.0-only, see the [LICENSE](LICENSE) file.

Copyright (C) 2021  Wibson - Daniel Fernandez, Rodrigo Irarrazaval
